// Як можна оголосити змінну у Javascript?
// У чому різниця між функцією prompt та функцією confirm?
// Що таке неявне перетворення типів? Наведіть один приклад.

let q1 = "Як можна оголосити змінну у Javascript?"
let q2 = "У чому різниця між функцією prompt та функцією confirm?"
let q3 = "Що таке неявне перетворення типів? Наведіть один приклад."

let a1 = "Використовувати ключеві слова let,const,var"
let a2 = "Функція confirm показує модальне вікно з питанням question та двома кнопками: ОК та Скасувати. Функція prompt показує модальне вікно з текстовим повідомленням, полем, куди відвідувач може ввести текст, та кнопками ОК/Скасувати."
let a3 = "Неявне перетворення типів, також відоме як примусове, це автоматичне перетворення типів при компіляції."
let a3_p2 = 'let sum = "5" + 5; // sum дорівнює "55"'

console.log(`${q1}\n\n${a1}`);
console.log(`${q2}\n\n${a2}`);
console.log(`${q3}\n\n${a3}\n\n${a3_p2}`);

let admin ;
let name = "Dmytro" ;
admin = name ;
console.log(`${admin}`);

let days = 3;
let hours = days * 24;
let minutes = hours * 60;
let seconds = minutes * 60;

console.log(`
    days = ${days}\n
    ${days} days = ${hours} hours
    ${hours} hours = ${minutes} minutes
    ${minutes} minutes = ${seconds} seconds
`);

let something = prompt("typy anithing", "i love snaks")
console.log(something);
